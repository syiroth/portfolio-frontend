import { createRouter, createWebHistory } from "vue-router";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "Home",
      component: () => import("../views/Home.vue"),
      meta: {
        hideHeader: true
      }
    },
    {
      path: "/projects",
      name: "Projects",
      component: () => import("../views/Projects.vue"),
      meta: {
        hideHeader: false
      }
    },
    {
      path: "/resume",
      name: "Resume",
      component: () => import("../views/Resume.vue"),
      meta: {
        hideHeader: false
      }
    },
    {
      path: "/articles",
      name: "Articles",
      component: () => import("../views/Articles.vue"),
      meta: {
        hideHeader: false
      }
    },
    {
      path: "/contact",
      name: "Contact",
      component: () => import("../views/Contact.vue"),
      meta: {
        hideHeader: false
      }
    },
  ],
  linkActiveClass: "current"
});

export default router;
